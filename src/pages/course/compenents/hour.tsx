import React from "react";
import { useNavigate } from "react-router-dom";
import styles from "./hour.module.scss";
import { durationFormat } from "../../../utils/index";
import { Card, Col, Row } from 'antd';

interface PropInterface {
  id: number;
  cid: number;
  title: string;
  duration: number;
  record: any;
  progress: number;
  name: string;
  poster: string;
}

export const HourCompenent: React.FC<PropInterface> = ({
  id,
  cid,
  title,
  duration,
  record,
  progress,
  name,
  poster
}) => {
  const navigate = useNavigate();
  return (


    <Card
      hoverable
      bordered
      className={styles["card"]}
      style={{ width: 250 }}
      title={name} extra={<a href="#" style={{ color: "white" }}>时长：{durationFormat(Number(duration))}</a>}>
      <div
        className={styles[""]}
        onClick={() => {
          navigate(`/course/${cid}/hour/${id}`);
        }}
      >
        <div className={styles["left-item"]}>
          <img src={poster} style={{ width: 200, height: 120 }}></img>
        </div>
        <div>视频详情：{name}({durationFormat(Number(duration))})</div>
        {/* <i className="iconfont icon-icon-video"></i> */}
        <div className={styles["title"]}>

        </div>
        {/* <div className="">
          {progress >= 0 && progress < 100 && (
            <>
              {progress === 0 && <div className={styles["link"]}>开始看</div>}
              {progress !== 0 && (
                <>
                 
                  <div className={styles["link"]}>继续看</div>
                </>
              )}
            </>
          )}
          {progress >= 100 && <div className={styles["complete"]}>已看完</div>}
        </div> */}
      </div>
    </Card>

  );
};
