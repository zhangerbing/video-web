import React, { useState, useEffect, useRef } from "react";
import styles from "./index.module.scss";
import { course, live } from "../../api/index";
import { Row, Col, Spin, Image, Progress, Modal } from "antd";
import { Empty } from "../../compenents";
import mediaIcon from "../../assets/images/commen/icon-medal.png";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import {
  Player,
  ControlBar,
  PlayToggle, // PlayToggle 播放/暂停按钮 若需禁止加 disabled
  ReplayControl, // 后退按钮
  ForwardControl,  // 前进按钮
  CurrentTimeDisplay,
  TimeDivider,
  PlaybackRateMenuButton,  // 倍速播放选项
  VolumeMenuButton
} from 'video-react';
import "video-react/dist/video-react.css"; // import css

type LastLearnModel = {
  [key: number]: LearnModel;
};

type LearnModel = {
  course: LastCourseModel;
  hour_record: HourRecordModel;
  last_learn_hour: LastHourModel;
  record: CourseRecordModel;
};

type LastCourseModel = {
  charge?: number;
  class_hour: number;
  created_at?: string;
  id: number;
  is_required: number;
  is_show?: number;
  short_desc: string;
  thumb: string;
  title: string;
};

type LastHourModel = {
  chapter_id: number;
  course_id: number;
  duration: number;
  id: number;
  rid: number;
  sort: number;
  title: string;
  type: string;
};

const LatestLearnPage = () => {
  // document.title = "最近学习";
  const navigate = useNavigate();
  const systemConfig = useSelector((state: any) => state.systemConfig.value);
  const [loading, setLoading] = useState<boolean>(false);
  const [courses, setCourses] = useState<LastLearnModel[]>([]);
  const [visiable, setVisiable] = useState(false);
  const [centered, setCentered] = useState(true);
  const videoRef = useRef<HTMLVideoElement>(null);

  const currentDepId = useSelector(
    (state: any) => state.loginUser.value.currentDepId
  );
  useEffect(() => {
    getCourses();
    setCentered(true)

  }, []);


  const getCourses = () => {
    setLoading(true);
    live.liveList(currentDepId).then((res: any) => {
      setCourses(res.data.data);
      setLoading(false);
    });
  };
  // const players = useState();

  const onOk = () => {
    console.log("编写自己的onOk逻辑");
    closeModal();
  };

  const closeModal = () => {
    videoRef.current?.pause();
    setVisiable(false);
  };


  const video = null;
  const [videoUrl, setVideoUrl] = useState("");
  const [title, setTitle] = useState("");
  const [poster, setPoster] = useState("")
  const clickVideo = (item: any) => {
    return () => {
      setVideoUrl(item.url)
      setTitle(item.title)
      setPoster(item.thumb)

      // title = item.title
      // videoUrl = item.url
      setVisiable(true)
    }
  }

  return (
    <div className="main-body">
      <div className={styles["content"]}>
        {loading && (
          <Row style={{ width: 1200 }}>
            <div className="float-left d-j-flex mt-50">
              <Spin size="large" />
            </div>
          </Row>
        )}
        {!loading && courses.length === 0 && (
          <Row style={{ width: 1200 }}>
            <Col span={24}>
              <Empty />
            </Col>
          </Row>
        )}
        {!loading &&
          courses.length > 0 &&
          courses.map((item: any, index: number) => (
            <div key={index}>
              {item && (
                <div
                  className={styles["item"]}
                  onClick={clickVideo(item)}
                >
                  <div style={{ width: 120 }}>
                    <Image
                      loading="lazy"
                      src={item.thumb}
                      width={120}
                      height={90}
                      style={{ borderRadius: 10 }}
                      preview={false}
                    />
                  </div>
                  <div className={styles["item-info"]}>
                    <div className={styles["top"]}>

                      <div className={styles["title"]}>直播标题：{item.title}</div>
                    </div>

                    <div className={styles["top2"]}>
                      <div className={styles["title"]}>直播简介：{item.show_desc}</div>
                    </div>
                    {/* {item.record && (
                      <>
                        {item.last_learn_hour && (
                          <div className={styles["record"]}>
                            上次看到：{item.last_learn_hour.title}
                          </div>
                        )}
                        <div className={styles["progress"]}>
                          {item.record.progress < 10000 && (
                            <Progress
                              percent={Math.floor(item.record.progress / 100)}
                              strokeColor="#FF4D4F"
                              trailColor="#F6F6F6"
                            />
                          )}
                          {item.record.progress >= 10000 && (
                            <>
                              <Image
                                loading="lazy"
                                width={24}
                                height={24}
                                src={mediaIcon}
                                preview={false}
                              />
                              <span className={styles["tip"]}>
                              
                              </span>
                            </>
                          )}
                        </div>
                      </>
                    )}
                    {!item.record && (
                      <>
                        {item.last_learn_hour && (
                          <div className={styles["record"]}>
                            上次学到：{item.last_learn_hour.title}
                          </div>
                        )}
                        <div className={styles["progress"]}>
                          <Progress
                            percent={1}
                            strokeColor="#FF4D4F"
                            trailColor="#F6F6F6"
                          />
                        </div>
                      </>
                    )} */}
                  </div>

                </div>
              )}
            </div>
          ))}
        <div className="modelClass">
          <Modal
            title={title}
            open={visiable}
            onOk={onOk}
            onCancel={closeModal}
            afterClose={closeModal}
            centered={centered}
          >
            <div>
              <Player
                ref={videoRef}
                preload='none'
                autoPlay='true'
                playsInline='true'
                src={videoUrl}
                poster={poster}
              >

                <ControlBar autoHide={false}>
                  <ReplayControl seconds={10} order={1.1} />
                  <PlayToggle />
                  <CurrentTimeDisplay order={4.1} />
                  <TimeDivider order={4.2} />
                  <PlaybackRateMenuButton rates={[5, 2, 1.5, 1, 0.5]} order={7.1} />
                  <VolumeMenuButton />
                </ControlBar>
              </Player>


              {/* {
              <live-player
                className={styles.player}
                video-url={videoUrl}
                fluent="true"
                live
                stretch="true"
                autoplay="true"
                controls
              />
            } */}

            </div>
          </Modal>
        </div>
      </div>
      <div className={styles["extra"]}>{systemConfig.pcIndexFooterMsg}</div>
    </div>
  );
};

export default LatestLearnPage;
